//
//  FoodSearchTableViewController.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 10/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class FoodSearchTableViewController: UITableViewController, UISearchResultsUpdating {
  var cuisine: Cuisine!
  var recipes: [RecipeSummary]?
  var showSearch: Bool?
  var searchText = ""
  var maxResultCount = 1
  var loadingState = false
  
  var filteredRecipes: [RecipeSummary]?
  var searchController: UISearchController!
  var spinner: UIActivityIndicatorView!
  
  @IBOutlet weak var loadMoreView: LoadMoreFooter!
  
  func filterRecipes(keyword: String?, cuisine: Cuisine?, maxResult: Int, callBck: (recipes:[RecipeSummary]) -> Void ) {
    // if loading more, show loading more view and hide spinner
    loadMoreView.hidden = !loadingState
    if !loadingState {
      spinner.startAnimating()
    }
    
    self.view.userInteractionEnabled = false
    
    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    dispatch_async(queue) {
      let group = dispatch_group_create()
      
      dispatch_group_async(group, queue) {
        RecipeModel.sharedInstance.getRecipesFromYummly(keyword, cuisine: cuisine, start: "0", maxResult: String(maxResult*10), errorCallback: self.errCallBck, callback: callBck)
      }
    }
  }
  
  func errCallBck(str: String) -> Void {
    println(str)
  }
  
  func callback(recipes: [RecipeSummary]) -> Void {
    if self.showSearch! && !searchController.searchBar.text.isEmpty {
      self.filteredRecipes = recipes
    } else {
      self.recipes = recipes
    }
    reloadTableAfterCallback()
  }
  
  func reloadTableAfterCallback() {
    self.spinner.stopAnimating()
    self.view.userInteractionEnabled = true
    tableView.reloadData()
    self.loadMoreView.hidden = !loadingState
  }
  
  func updateSearchResultsForSearchController(searchController: UISearchController) {
    filteredRecipes = []
    maxResultCount = 1
    searchText = searchController.searchBar.text
    filterRecipes(searchText, cuisine: cuisine, maxResult: maxResultCount, callBck: callback)
  }
  
  func createSpinner() {
    spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
    spinner.color = UIColor.blackColor()
    spinner.hidesWhenStopped = true
    spinner.center = self.tableView.center
    
    self.view.addSubview(spinner)
  }
  
  override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    
    // load more recipe if scrolled to the end of the table view
    if (bottomEdge >= scrollView.contentSize.height) {
      loadingState = true
      maxResultCount = maxResultCount + 1
      filterRecipes(searchText, cuisine: cuisine, maxResult: maxResultCount, callBck: callback)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController!.setNavigationBarHidden(false, animated:true)
    var backBtn:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
    backBtn.addTarget(self, action: "backToRoot:", forControlEvents: UIControlEvents.TouchUpInside)
    backBtn.setImage(UIImage(named: "back.png"), forState: UIControlState.Normal)
    backBtn.frame = CGRectMake(0, 0, 20, 20)

    var myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: backBtn)
    self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
    
    self.tableView.contentInset = UIEdgeInsetsMake(-8, -15, -15, -25);
    loadMoreView.hidden = true
    createSpinner()
    
    recipes = []
    filteredRecipes = []
    filterRecipes(searchText, cuisine: cuisine, maxResult: maxResultCount, callBck: callback)
    
    searchController = UISearchController(searchResultsController: nil)
    searchController.searchResultsUpdater = self
    searchController.dimsBackgroundDuringPresentation = false
    
    searchController.searchBar.sizeToFit()
    if(self.showSearch!) {
      tableView.tableHeaderView = searchController.searchBar
    }
    
    // Sets this view controller as presenting view controller for the search interface
    definesPresentationContext = true
  }
  
    func backToRoot(sender:UIBarButtonItem){
        self.navigationController!.popToRootViewControllerAnimated(true)
    }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  // MARK: - Table view data source
  
  // Set height of the table view row
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return 200
  }
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if self.showSearch! && !searchController.searchBar.text.isEmpty {
      return filteredRecipes!.count
    } else {
      return recipes!.count
    }
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("FoodCell", forIndexPath: indexPath) as! FoodCell
    
    var recipe: RecipeSummary
    
    if !searchController.searchBar.text.isEmpty {
      recipe = filteredRecipes![indexPath.row]
    } else {
      recipe = recipes![indexPath.row]
    }
    let url = NSURL(string: recipe.getImageBySize("600"))
    let data = NSData(contentsOfURL: url!)
    
    cell.foodImage.image = UIImage(data: data!)
    cell.foodImage.accessibilityLabel = recipe.getRecipeName()
    cell.foodName.text = recipe.getRecipeName()
    return cell
  }
  
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    self.performSegueWithIdentifier("foodDetail-1", sender: tableView)
  }

  // MARK: - Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "foodDetail-1" {
      let foodDetailVC = segue.destinationViewController as! ViewController
      let indexPath = self.tableView.indexPathForSelectedRow()!
      var selectedRecipe: RecipeSummary
      
      if !searchController.searchBar.text.isEmpty {
        selectedRecipe = filteredRecipes![indexPath.row]
      } else {
        selectedRecipe = recipes![indexPath.row]
      }
      
      foodDetailVC.title = selectedRecipe.getRecipeName()
      foodDetailVC.recipeSummaryId = selectedRecipe.getId()
    }
  }
  
}
