//
//  CuisineCell.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 7/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class CuisineCell: UICollectionViewCell {
  
  @IBOutlet weak var cuisineImage: UIImageView!
  
}
