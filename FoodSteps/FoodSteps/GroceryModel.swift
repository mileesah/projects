//
//  GroceryModel.swift
//  FoodSteps
//
//  Created by Melisaa Ferenal on 5/11/15.
//  Copyright (c) 2015 jeli. All rights reserved.
//  
//  Code Based on Tute 7 example

import Foundation
import CoreData
import UIKit

class GroceryModel{
    
    var groceryList : [GroceryItem]
    
    private struct Static
    {
        static var instance: GroceryModel?
    }
    
    
    
    class var sharedInstance: GroceryModel
    {
        if !(Static.instance != nil)
        {
            Static.instance = GroceryModel()
        }
        return Static.instance!
    }
    
    
    private init(){
        groceryList = []
    }
    
    
    func getGroceryItem(indexPath: NSIndexPath) -> GroceryItem
    {
        return groceryList[indexPath.row]
    }
    
    func newGroceryItem(name: String, qty:String)
    {
        // Get a reference to your App Delegate
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        // Get a reference to a ManagedObjectContext for interacting with
        // the underlying database
        let managedContext = appDelegate.managedObjectContext!
        // Get a entity from the database that represents the table your are
        // wishing to work with
        let entity = NSEntityDescription.entityForName("GroceryItem",
            inManagedObjectContext: managedContext)
        // Create an object based on the Entity
        let item = GroceryItem(entity: entity!,
            insertIntoManagedObjectContext:managedContext)
        // Set any properties
        item.setValue(name, forKey: "name")
        item.setValue(qty, forKey: "qty")
        item.setValue(false, forKey: "done")

        // Check for errors and handle
        var error: NSError?
        if !managedContext.save(&error)
        {
            println("Could not save \(error), \(error?.userInfo)")
        }
        // Append the data to the in memory model
        self.groceryList.append(item)
        
    }
    
    func loadGroceryItems()
    {
        // Get a reference to your App Delegate
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        // Get a reference to a ManagedObjectContext for interacting with
        // the underlying database
        let managedContext = appDelegate.managedObjectContext!
        // Retrieve all the records in the table
        let fetchRequest = NSFetchRequest(entityName:"GroceryItem")
        var error: NSError?
        let fetchedResults = managedContext.executeFetchRequest(fetchRequest, error:
            &error) as! [GroceryItem]?
        // Assign the results to the Model
        if let results = fetchedResults
        {
            groceryList = results
        }
        else
        {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }
    
    func updateGroceryItem(name: String, qty:String, isDone: Bool, existing: GroceryItem?)
    {
        // Get a reference to your App Delegate
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        // Get a reference to a ManagedObjectContext for interacting with
        // the underlying database
        let managedContext = appDelegate.managedObjectContext!
        // Get a entity from the database that represents the table your are
        // wishing to work with
        let entity = NSEntityDescription.entityForName("GroceryItem",
        inManagedObjectContext: managedContext)
        if((existing) != nil)
        {
            existing!.name = name
            existing!.qty = qty
            existing!.done = isDone
        }
        // Check for errors and save
        var error: NSError?
        if !managedContext.save(&error)
        {
            println("Could not save \(error), \(error?.userInfo)")
        }
    }
    
    func deleteGroceryItem(indexPath: NSIndexPath)
    {
            let item = groceryList[indexPath.row]
            // Get a reference to your App Delegate
            let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate

            // Get a reference to a ManagedObjectContext for interacting with
            // the underlying database
            let managedContext = appDelegate.managedObjectContext!
            managedContext.deleteObject(item)
            groceryList.removeAtIndex(indexPath.row)
            var error: NSError?
            if !managedContext.save(&error)
            {
                abort()
            }
    }
    
}