//
//  Recipe.swift
//  FoodSteps
//
//  Created by admin  on 9/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import Foundation

class RecipeSummary
{
    private var imageUrlBySize:[Int:String]!
    private var sourceDisplayName:String!
    private var ingredient:[String]!
    private var id:String
    private var smallImageUrls:[String]!
    private var recipeName:String!
    private var totalTimeInSeconds:Int!
    private var course:[String]!
    private var cuisine:[String]!
    private var flavors:[String:Double]!
    private var rating:Int!
    
    
    init(imageUrlBySize:[Int:String],sourceDisplayName:String,ingredient:[String],id:String,smallImageUrls:[String],recipeName:String,totalTimeInSeconds:Int,flavors:[String:Double],rating:Int, course:[String], cuisine:[String])
    {
        self.imageUrlBySize=imageUrlBySize;
        self.sourceDisplayName=sourceDisplayName;
        self.ingredient=ingredient;
        self.id=id;
        self.smallImageUrls=smallImageUrls;
        self.recipeName=recipeName;
        self.totalTimeInSeconds=totalTimeInSeconds;
        self.course=course;
        self.cuisine=cuisine;
        self.flavors=flavors;
        self.rating=rating;
    }
    
    func getImageUrlBySize()->[Int:String]{
        return imageUrlBySize;
    }
    
    func getSourceDisplayName()->String{
        return sourceDisplayName;
    }
    
    func getIngredient()->[String]{
        return ingredient;
    }
    
    func getId()->String{
        return id;
    }
    
    func getSmallImageUrls()->[String]{
        return  smallImageUrls;
    }
    
    func getRecipeName()->String{
        return recipeName;
    }
    
    func getTotalTimeInSeconds()->Int{
        return totalTimeInSeconds;
    }
    
    func getFlavors()->[String:Double]{
        return flavors;
    }
    
    func getRating()->Int{
        return rating;
    }
    
    func getImageBySize(sizeNum:String) -> String{
        var size = "=s"+sizeNum+"-c"
        var str = getImageUrlBySize().values.first!
        var regex:NSRegularExpression = NSRegularExpression(pattern: "=.*", options: NSRegularExpressionOptions.CaseInsensitive, error: nil)!
        var modString = regex.stringByReplacingMatchesInString(str, options: nil, range: NSMakeRange(0, count(str)), withTemplate: size)
        
        return modString

    }
    
}