//
//  GroceryListTableViewController.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 18/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class GroceryListTableViewController: UITableViewController, CheckboxDelegate {
    
  let groceryModel: GroceryModel = GroceryModel.sharedInstance;
  
  func createCheckboxes(cell: UITableViewCell, item: GroceryItem) {
    var frame = CGRectMake(0, 0, 40, 40)
    var checkbox = Checkbox(frame: frame, selected: item.isDone())
    checkbox.mDelegate = self
    checkbox.tag = find(groceryModel.groceryList, item)!
      
    cell.addSubview(checkbox)
  }
  
  func didSelectCheckbox(state: Bool, identifier: Int) {
    let item = groceryModel.groceryList[identifier]
    groceryModel.updateGroceryItem(item.getName(), qty: item.getQty(), isDone: state, existing: item)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationController!.setNavigationBarHidden(false, animated:true)
    var backBtn:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as! UIButton
    backBtn.addTarget(self, action: "backToRoot:", forControlEvents: UIControlEvents.TouchUpInside)
    backBtn.setImage(UIImage(named: "back.png"), forState: UIControlState.Normal)
    backBtn.frame = CGRectMake(0, 0, 20, 20)
    var myCustomBackButtonItem:UIBarButtonItem = UIBarButtonItem(customView: backBtn)
    self.navigationItem.leftBarButtonItem  = myCustomBackButtonItem
    groceryModel.loadGroceryItems()
    GroceryModel.sharedInstance.loadGroceryItems()
  }
    func backToRoot(sender:UIBarButtonItem){
        self.navigationController!.popToRootViewControllerAnimated(true)
    }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  // MARK: - Table view data source
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return groceryModel.groceryList.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("GroceryItemCell", forIndexPath: indexPath) as! GroceryItemCell
    
    let groceryItem = groceryModel.groceryList[indexPath.row]
    createCheckboxes(cell, item: groceryItem)
    cell.groceryItemLabel.text = groceryItem.getName()
    
    return cell
  }

  override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == UITableViewCellEditingStyle.Delete {
        groceryModel.deleteGroceryItem(indexPath)
      tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
    }
  }
}

