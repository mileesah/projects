//
//  IngredientsTableViewController.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 12/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class IngredientsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  var recipe: Recipe!
  var ingredients: [String]!
  var spinner: UIActivityIndicatorView!
  
  @IBOutlet weak var ingredientTV: UITableView!
  
  func loadRecipeById(recipeId: String) {
    spinner.startAnimating()
    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    
    dispatch_async(queue) {
      let group = dispatch_group_create()
      
      dispatch_group_async(group, queue) {
        RecipeModel.sharedInstance.getRecipe(recipeId, errorCallback: self.errCallBck, callback: self.callback)
      }
    }
  }
  
  func errCallBck(str: String) -> Void {
    println(str)
  }
  
  func callback(recipe: Recipe!) -> Void {
    self.recipe = recipe
    self.ingredients = recipe.getIngredientLines()
    spinner.stopAnimating()
    ingredientTV.reloadData()
  }
  
  override func viewDidLoad() {
    self.recipe = nil
    self.ingredients = []
    
    super.viewDidLoad()
    self.ingredientTV.delegate = self;
    self.ingredientTV.dataSource = self;
    
    var parentVC = self.parentViewController?.parentViewController as! ViewController
    spinner = parentVC.containerSpinner
    loadRecipeById(parentVC.recipeSummaryId)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  // MARK: - Table view data source
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return ingredients.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("IngredientCell", forIndexPath: indexPath) as! IngredientCell
    
    let ingredient = ingredients[indexPath.row]
    cell.addHintLbl.text = "1X" + ingredients[indexPath.row] + " added"
   
    
    cell.ingredientLabel.text = ingredient
  
    cell.addButton.accessibilityLabel = "Add "+ingredient + " Button";
    cell.addButton.accessibilityHint = "click to add "+ingredient+" into grocery list"
    return cell
  }
  
}
