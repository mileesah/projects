//
//  StepsViewController.swift
//  FoodSteps
//
//  Created by Jennifer Lius on 13/05/2015.
//  Copyright (c) 2015 jeli. All rights reserved.
//

import UIKit

class StepsViewController: UIViewController, UIWebViewDelegate {
  var recipe: Recipe!
  var stepsURL: String!
  var spinner: UIActivityIndicatorView!
  
  @IBOutlet weak var stepWV: UIWebView!
  
  func loadRecipeById(recipeId: String) {
    spinner.startAnimating()
    
    let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    
    dispatch_async(queue) {
      let group = dispatch_group_create()
      
      dispatch_group_async(group, queue) {
        RecipeModel.sharedInstance.getRecipe(recipeId, errorCallback: self.errCallBck, callback: self.callback)
      }
    }
  }
  
  func errCallBck(str: String) -> Void {
    println(str)
  }
  
  func callback(recipe: Recipe!) -> Void {
    self.recipe = recipe
    self.stepsURL = recipe.getSource()
    
    let url = NSURL(string: self.stepsURL)
    let urlRequest = NSURLRequest(URL: url!)
    self.stepWV.loadRequest(urlRequest)
  }
  
  func webViewDidFinishLoad(webView: UIWebView) {
    spinner.stopAnimating()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.stepWV.delegate = self;
    
    var parentVC = self.parentViewController?.parentViewController as! ViewController
    spinner = parentVC.containerSpinner
    loadRecipeById(parentVC.recipeSummaryId)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}
