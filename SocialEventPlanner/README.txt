Assignment 1
Mobile Application Development (COSC2309_1450)

AUTHOR
Melissa Ferenal
3497490


Social Event Planner

Views
- Daily
	Lists all scheduled events for the selected date
	Can be accessed at the overflow menu at the weekly view 
- Weekly
	Lists the dates of the selected week
	Can be accessed at the overflow menu at the daily view

How to edit an event

Daily view
	Click on the event and update the details

Weekly View
	Long click an event row and select edit on the pop up menu


How to delete an event

Daily view
	Click on the event and click on the trash icon at the top right side of the action bar

Weekly View
	Long click an event row and select delete on the pop up menu