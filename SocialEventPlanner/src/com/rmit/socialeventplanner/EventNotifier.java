package com.rmit.socialeventplanner;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.rmit.socialeventplanner.R;
import com.rmit.socialeventplanner.model.Event;
import com.rmit.socialeventplanner.model.dao.DBModel;

import com.rmit.socialeventplanner.view.SocialEventPlannerActivity;
import com.rmit.socialeventplanner.view.model.EventAdapter;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class EventNotifier extends Service 
implements
GooglePlayServicesClient.ConnectionCallbacks,
GooglePlayServicesClient.OnConnectionFailedListener,
LocationListener
{

	public static EventAdapter eventAdapter;
	
	public static EventNotifier _service;
	private DBModel db;
	private static int timer = 60000;
	private List<Event> eventList;
	
	private int secondsToUpdateList = 10;
	private int currentsecondsToUpdateList = 0;
	
	private static String CURRENT_LOCATION_LAT;
	private static String CURRENT_LOCATION_LON;
	
	public LocationClient mLocationClient;
	
	public static LocationManager mlocationManager = null;
	
	public EventNotifier() {
			Log.e("Location", "Start Service");
		    db = new DBModel(this);
		    Log.e("DB", "Created");
		    
		    //try{
		   // 	mlocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		   // }catch(Exception ex){
		   // 	Log.e("ERROR", "Assign mlocationManager fail");
		   // }
		    
			try{
				
				new LoadEvents().execute("");

			}catch(Exception ex){
				ex.toString();
			}
			
	}
	
	@Override
	public void onCreate() {
		mlocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		mlocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		
		mLocationClient = new LocationClient(this, this, this);
		mLocationClient.connect();

	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}
	
	@Override
	public void onDestroy() {
		
	}
	
	private void SendNotification(String message, String title, boolean action) {
		Intent mainIntent = new Intent(this, SocialEventPlannerActivity.class);
		
		PendingIntent contentIntent = PendingIntent.getActivity(this,
												                0, mainIntent,
												                PendingIntent.FLAG_CANCEL_CURRENT);
		NotificationCompat.Builder mBuilder =
        	    new NotificationCompat.Builder(this)
        	    .setSmallIcon(R.drawable.sep_logo)
        	    .setContentTitle(title)
        	    .setContentText(message)
        	    .setAutoCancel(true)
        	    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		
		if(action)
			mBuilder.setContentIntent(contentIntent);
		
        NotificationManager mNotifyMgr = 
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        
        mNotifyMgr.notify(1, mBuilder.build());
        
        
	} 
	
	private  Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			//SendNotification("Event","Event", true );
			if(secondsToUpdateList != currentsecondsToUpdateList){
				currentsecondsToUpdateList++;
			}else{
				updateEventList();
				currentsecondsToUpdateList = 0;
			}
			
			
			checkEvents();
			
			
			mHandler.sendEmptyMessageDelayed(1, timer);
			if(timer == 2000){
				timer = 60000;
			}
		}
	};
	
	private void updateEventList(){
		eventList = db.getEvents();
	}
	
	private void getDBdata(){
		  db.pupolateAllEvents();
		  List<Event> e = db.getEvents();
		  try{
      		eventAdapter.clear();
  			eventAdapter.addAll(e);
      		eventAdapter.notifyDataSetChanged();
      		updateEventList();
      	}catch (Exception ex){
      		eventList = e; 
      	}
      
			mHandler.sendEmptyMessageDelayed(1, 5000);
	}
	
	@SuppressLint("NewApi")
	private void checkEvents(){
		if(eventList == null)
			return;
		
		if(eventList.size() < 1)
			return;
		
		for(Event evt : eventList){

				Calendar currentDate = Calendar.getInstance();
				Calendar eventday = DateToCalendar(evt.getDate());
				
		        Date currentTime = new Date();
		        Date eventSetTime = evt.getDate();
		        long subtractedTime = getDateDiff(eventSetTime,currentTime,TimeUnit.MINUTES);
		        
		        //if(test == -59){
		        	
		        	
		        	String eventName = evt.getTitle();
		        	String location = evt.getLocation();
		        	String timeDifference = subtractedTime + "";
		        	
		        	if(location != null){
		        		
		        		if(CURRENT_LOCATION_LAT != null){
		        			timer = 60000;
		        			new CreateNotification().execute(location, eventName, timeDifference);
		        		}
		        	}
		        //}   
		}
	}
	
	
	 public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
         long diffInMillies = date2.getTime() - date1.getTime();
         return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
     }
	
	public static Calendar DateToCalendar(Date date){ 
		  Calendar cal = Calendar.getInstance();
		  cal.setTime(date);
		  return cal;
	}
	
	// LoadEvents() AsyncTask
    private class LoadEvents extends AsyncTask<String, Object, List<Event>> {
 
        @Override
        protected List<Event> doInBackground(String... params) {
        	//if(!SocialEventPlanner.getInstance().isOpen()){
                db.pupolateAllEvents();
                //SocialEventPlanner.getInstance().setOpen(true);
        	//}
            return db.getEvents();
        }
 
        @Override 
        protected void onPostExecute(List<Event> e) {
        	String ecount = e.size() + "";
        	String ecounts = ecount;
        	try{
        		eventAdapter.clear();
    			eventAdapter.addAll(e);
        		eventAdapter.notifyDataSetChanged();
        		updateEventList();
        	}catch (Exception ex){
        		eventList = e; 
        	}
        	Log.e("Async", "onPostExecute LoadEvents");
        	
        	try{
        		Thread.sleep(5000);
        		mHandler.sendEmptyMessageDelayed(1, 5000); 
        	}catch(Exception ex) {
        		
        	}
        	
        }
    } 
    
    
	  public List<Address> GetAdrress(double latitude, double longitude){
		  
		  	Geocoder geocoder = new Geocoder(EventNotifier.this, Locale.getDefault());
		  	List<Address> addresses = null;
			try {
				addresses = geocoder.getFromLocation(latitude, longitude,1);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  	return addresses;
	  }


	  @Override
	  public void onTaskRemoved(Intent rootIntent) {
	      // TODO Auto-generated method stub
	      Intent restartService = new Intent(getApplicationContext(),
	              this.getClass());
	      restartService.setPackage(getPackageName());
	      PendingIntent restartServicePI = PendingIntent.getService(
	              getApplicationContext(), 1, restartService,
	              PendingIntent.FLAG_ONE_SHOT);
	      AlarmManager alarmService = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
	      alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() +1000, restartServicePI);

	  }

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		Log.e("Location", "Newtork assign");
		try{
		double lat = location.getLatitude();
		double lon = location.getLongitude();
		CURRENT_LOCATION_LAT = lat + "";
		CURRENT_LOCATION_LON = lon + "";
		Log.e("Location", "Newtork assign done");
		}catch (Exception ex){
			Log.e("Location", "Newtork assign fail");
		}
		
		//SendNotification("1 hour berfore " + "", CURRENT_LOCATION_LAT + ", " + CURRENT_LOCATION_LON, true);
		
	}



	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
	}
	
	
	private class GetLatLongRequest extends AsyncTask<String, String, String> {
		 
        @Override
        protected String doInBackground(String... params) {
        	String s = params[0];
        	String result = LocationHelper.getLatLongFromAddress(s);
   			
            return result;
        }
 
        @Override
        protected void onPostExecute(String e) {
        	
        }
    } 
	
	private class GetDistanceRequest extends AsyncTask<String, String, String> {
		 
        @Override
        protected String doInBackground(String... params) {
        	String origin = params[0];
        	String distination = params[1];
        	String result = LocationHelper.getDistanceFromAddres(origin, distination);
   			
            return result;
        }
 
        @Override 
        protected void onPostExecute(String e) {
        
        }
    } 
	
	private class CreateNotification extends AsyncTask<String, String, String> {
		 
        @Override
        protected String doInBackground(String... params) {
       	
    	String venue = params[0];
    	String eventName = params[1];
    	String timeDiff = params[2];
    	String result = "";
    	result = eventName + "|";
	    	try{
	       	 double cLat = Double.parseDouble(CURRENT_LOCATION_LAT);
	       	 double cLon = Double.parseDouble(CURRENT_LOCATION_LON);
	       	 
	       	String distanceDescription = LocationHelper.getDistanceFromAddres(cLat + "," + cLon, venue);
	       	result = result + distanceDescription + "|" + timeDiff + "|" + venue;
	       	 
	    	}catch(Exception e){
	             result = result + "error";
	    	}
	    	return result;
        }
 
        @Override 
        protected void onPostExecute(String e) {
        	
        	String[] event = e.split("\\|");
        	String eventName = event[0];
        	String data = event[1];
        	
        	String description = "";
        	
        	if(!data.equals("error")){
        		
        		String distance = event[1];
        		String durationText = event[2];
        		
        		String durationValue = event[3];
        		String timeDiff = event[4];
        		String Venue = event[5];
        		
        		String positiveTime = timeDiff.replace("-", "");
        	
        		Log.d("durationValue", durationValue);
        		Log.d("timeDiff", timeDiff);
        		
        		if(durationValue.equals(positiveTime)){
        			SendNotification(durationText + " to travel " + Venue , distance + " away from " + eventName , true);
            		timer = 2000;
        		}else{
        			//for testting
        			//SendNotification(durationText + " to travel " + Venue + " : " + timeDiff , distance + " away from " + eventName , true);
        			
        		}
        		
        	}
        	else{
        		Log.d("Notify Error", "Notify Error");
        	}
        	
        	
        	
        }
    }


	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		Log.e("Location", "API assign");
		if(mLocationClient.getLastLocation() != null){
			 Location mCurrentLocation = mLocationClient.getLastLocation();
			 double Lat = mCurrentLocation.getLatitude();
			 double Lon = mCurrentLocation.getLongitude();
			 CURRENT_LOCATION_LAT = Lat + "";
			 CURRENT_LOCATION_LON = Lon + "";
			 Log.e("Location", "API assign done");
		}else{
			Log.e("Location", "API assign fail");
		}
		
		
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	} 
}

