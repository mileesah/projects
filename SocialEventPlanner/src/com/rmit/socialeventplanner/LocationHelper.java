package com.rmit.socialeventplanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import com.rmit.socialeventplanner.model.Event;

public class LocationHelper {
	
	
	
	 public static String _result = "";
	  public static class getLocation extends AsyncTask<String, String, String> {
		  
	        @Override
	        protected String doInBackground(String... params) {
	        	String data = params.toString();
	            return getLatLongFromAddress(data);
	        }
	 
	        @Override
	        protected void onPostExecute(String e) {
	        	String result = e;
	        	_result = result;
	        }
	    }
	  
	  public static String getLatLongFromAddress(String youraddress) {
		    String uri = "http://maps.google.com/maps/api/geocode/json?address=" +
		                  youraddress + "&sensor=false";
		    HttpGet httpGet = new HttpGet(uri);
		    HttpClient client = new DefaultHttpClient();
		    HttpResponse response;
		    StringBuilder stringBuilder = new StringBuilder();

		    try {
		        response = client.execute(httpGet);
		        HttpEntity entity = response.getEntity();
		        InputStream stream = entity.getContent();
		        int b;
		        while ((b = stream.read()) != -1) {
		            stringBuilder.append((char) b);
		        }
		    } catch (ClientProtocolException e) {
		        e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }

		    JSONObject jsonObject = new JSONObject();
		    try {
		        jsonObject = new JSONObject(stringBuilder.toString());

		        double lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
		            .getJSONObject("geometry").getJSONObject("location")
		            .getDouble("lng");

		        double lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
		            .getJSONObject("geometry").getJSONObject("location")
		            .getDouble("lat");

		        Log.d("latitude", "" + lat);
		        Log.d("longitude", "" + lng);
		        
		        return lng + ", " + lat;
		        
		    } catch (JSONException e) {
		        e.printStackTrace();
		    }
			return "0.0, 0.0";

		}
	  
	  
	  public static String getDistanceFromAddres(String origin, String destination) {
		    String uri = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+ origin +"&destinations=" + destination;
		    HttpGet httpGet = new HttpGet(uri);
		    HttpClient client = new DefaultHttpClient();
		    HttpResponse response;
		    StringBuilder stringBuilder = new StringBuilder();

		    try {
		        response = client.execute(httpGet);
		        HttpEntity entity = response.getEntity();
		        InputStream stream = entity.getContent();
		        int b;
		        while ((b = stream.read()) != -1) {
		            stringBuilder.append((char) b);
		        }
		    } catch (ClientProtocolException e) {
		        e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }

		    JSONObject jsonObject = new JSONObject();
		    try {
		        jsonObject = new JSONObject(stringBuilder.toString());

		        String distance = ((JSONArray)jsonObject.get("rows")).getJSONObject(0)
		        	.getJSONArray("elements").getJSONObject(0).getJSONObject("distance")
		            .getString("text");

		        String durationText = ((JSONArray)jsonObject.get("rows")).getJSONObject(0)
		        		.getJSONArray("elements").getJSONObject(0).getJSONObject("duration")
			            .getString("text");
		        
		        double durationValue = ((JSONArray)jsonObject.get("rows")).getJSONObject(0)
		        		.getJSONArray("elements").getJSONObject(0).getJSONObject("duration")
			            .getDouble("value");
		        
		        double doubleMinutes = (durationValue / 60);
		        int ArrayMinutes = (int) doubleMinutes; 
		        String durationMinutes = ArrayMinutes + "";
		        
		        return distance + "|" + durationText + "|" + durationMinutes;
		        
		    } catch (JSONException e) {
		        e.printStackTrace();
		    }
			return "cannot calculate distance";

		}
	  
	  public static void getSecondsValue(){
		  
	  }

	// LoadEvents() AsyncTask
	    
	
}
