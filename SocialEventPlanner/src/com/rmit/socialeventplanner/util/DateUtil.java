/**
 * A utility class to handle date related operations
 */
package com.rmit.socialeventplanner.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	public static String formatCalendarDate(Calendar cal) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
		return sdf.format(cal.getTime());
	}

	public static String formatCalendarTime(Calendar cal) {
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());
		return sdf.format(cal.getTime());
	}

	public static Date stringToDate(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault());

		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;

	}

	public static String dateToString(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault());

		return sdf.format(date);

	}
	
	public static String formatCalendarDateToHeaderString(Calendar cal) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
		return sdf.format(cal.getTime());
	}
	
	public static String getMonthYear(Calendar cal){
		SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
		return sdf.format(cal.getTime());
	}
}
