/**
 * a time picker widget
 */
package com.rmit.socialeventplanner.view;

import java.util.Calendar;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.EditText;

import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.controller.TimePickerController;
import com.rmit.socialeventplanner.model.EventModel;

public class EventTimePicker extends DialogFragment{

	
	private EditText timeTxt;
	public EventTimePicker(EditText timeTxt){
		this.timeTxt=timeTxt;
	}

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the selected date as the default date in the picker
        Calendar selectedDate = SocialEventPlanner.getInstance().getSelectedDate();
        

        return new TimePickerDialog(getActivity(), new TimePickerController(timeTxt), 
        		selectedDate.get(Calendar.HOUR_OF_DAY), selectedDate.get(Calendar.MINUTE), false);
    }
}
