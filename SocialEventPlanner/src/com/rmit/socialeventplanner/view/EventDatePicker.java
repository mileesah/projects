/**
 * A date picker widget
 */
package com.rmit.socialeventplanner.view;

import java.util.Calendar;

import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.controller.DatePickerController;
import com.rmit.socialeventplanner.model.EventModel;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.EditText;
import android.app.DialogFragment;

public class EventDatePicker extends DialogFragment {

	private EditText dateTxt;

	public EventDatePicker(EditText dateTxt) {
		this.dateTxt = dateTxt;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Calendar cal = SocialEventPlanner.getInstance().getSelectedDate();
		return new DatePickerDialog(getActivity(), new DatePickerController(
				dateTxt), cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
				cal.get(Calendar.DAY_OF_MONTH));
	}
}
