/**
 * The default view of the application
 * This is the daily event view
 */
package com.rmit.socialeventplanner.view;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.rmit.socialeventplanner.EventNotifier;
import com.rmit.socialeventplanner.LocationHelper;
import com.rmit.socialeventplanner.R;
import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.controller.DateNavigationController;
import com.rmit.socialeventplanner.model.Event;
import com.rmit.socialeventplanner.model.EventModel;
import com.rmit.socialeventplanner.model.dao.DBModel;
import com.rmit.socialeventplanner.util.DateUtil;
import com.rmit.socialeventplanner.view.model.EventAdapter;

public class SocialEventPlannerActivity extends Activity implements LocationListener {

	List<Event> eventList;
	ListView eventListView;
    DBModel db;

	public static int DAILY_CODE = 12;
	
	private TextView selected_date_txt;
	private ImageButton left_btn;
	private ImageButton right_btn;
	private EventModel modelInstance;

	LocationManager locationManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
 		setContentView(R.layout.event_list_view);
		eventListView = (ListView) findViewById(R.id.event_list);
		
		//try{
	    //locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		//EventNotifier.mlocationManager = locationManager; 
		//locationManager.getLastKnownLocation(WIFI_SERVICE);
		//Log.e("ERROR", "passing locationManager success");
		//}catch(Exception e){
		//	Log.e("ERROR", "passing locationManager fail");
		//}

		
		db = new DBModel(SocialEventPlannerActivity.this);
		selected_date_txt = (TextView) findViewById(R.id.selected_date_txt);
		left_btn = (ImageButton) findViewById(R.id.left_btn);
		right_btn = (ImageButton) findViewById(R.id.right_btn);
		EventNotifier.eventAdapter = new EventAdapter(SocialEventPlannerActivity.this, new ArrayList<Event>(),DAILY_CODE );

		left_btn.setOnClickListener(new DateNavigationController(EventNotifier.eventAdapter,
				selected_date_txt, modelInstance,
				DateNavigationController.LEFT_NAVIGATION));
		right_btn.setOnClickListener(new DateNavigationController(EventNotifier.eventAdapter,
				selected_date_txt, modelInstance,
				DateNavigationController.RIGHT_NAVIGATION));

		selected_date_txt.setText(DateUtil
				.formatCalendarDateToHeaderString(SocialEventPlanner.getInstance()
						.getSelectedDate()));
		eventListView.setAdapter(EventNotifier.eventAdapter);

		
		Intent intent = new Intent(SocialEventPlannerActivity.this, EventNotifier.class);
		this.startService(intent);

	}
 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_add:
			Intent addEvent = new Intent(this, EventFormActivity.class);
			addEvent.putExtra("requestCode", EventFormActivity.ADD_EVENT);
			startActivityForResult(addEvent, EventFormActivity.ADD_EVENT);
			break;
		case R.id.action_view_week:
			Intent weekActivity = new Intent(this, WeekViewActivity.class);
			startActivity(weekActivity);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == EventFormActivity.ADD_EVENT
				|| resultCode == EventFormActivity.EDIT_EVENT_CODE) {

			selected_date_txt.setText(DateUtil
					.formatCalendarDateToHeaderString(SocialEventPlanner.getInstance()
							.getSelectedDate()));
			EventNotifier.eventAdapter.clear();
			EventNotifier.eventAdapter.addAll(db.getEvents());
			EventNotifier.eventAdapter.notifyDataSetChanged();

		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		double lat = location.getLatitude();
		double lon = location.getLatitude();
		String latString = lat + "";
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	
}
