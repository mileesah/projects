/**
 * Weekly view 
 */
package com.rmit.socialeventplanner.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.rmit.socialeventplanner.R;
import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.controller.WeekNavigationController;
import com.rmit.socialeventplanner.model.Event;
import com.rmit.socialeventplanner.model.EventModel;
import com.rmit.socialeventplanner.model.dao.DBModel;
import com.rmit.socialeventplanner.util.DateUtil;
import com.rmit.socialeventplanner.view.model.EventAdapter;

public class WeekViewActivity extends Activity {

	public static int WEEKLY_CODE = 13;

	public static ListView weekEventList;
	private EventModel modelInstance;
	private List<Event> eventList;
	private Calendar selectedDate;
	private TextView sun_date_head;
	private TextView mon_date_head;
	private TextView tue_date_head;
	private TextView wed_date_head;
	private TextView thu_date_head;
	private TextView fri_date_head;
	private TextView sat_date_head;
	public static Drawable selectedDrawable;
	public static Drawable defaultDrawable;
	private LinearLayout sunday_box;
	private LinearLayout monday_box;
	private LinearLayout tuesday_box;
	private LinearLayout wednesday_box;
	private LinearLayout thursday_box;
	private LinearLayout friday_box;
	private LinearLayout satday_box;
	private EventAdapter eventAdapter;
	private DBModel db;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.weekly_activity);
		db = new DBModel(WeekViewActivity.this);

		weekEventList = (ListView) findViewById(R.id.weekEventList);

		modelInstance = SocialEventPlanner.getInstance().getEventModel();
		eventAdapter = new EventAdapter(this, new ArrayList<Event>(db.getEvents()),
				WEEKLY_CODE);
		if (selectedDrawable == null) {
			selectedDrawable = getResources().getDrawable(
					R.drawable.week_shapes_sel);
		}
		if (defaultDrawable == null) {
			defaultDrawable = getResources()
					.getDrawable(R.drawable.week_shapes);
		}
		weekEventList.setAdapter(eventAdapter);
		populateWeekDates();
		registerForContextMenu(weekEventList);

	}

	private void populateWeekDates() {
		selectedDate = SocialEventPlanner.getInstance().getSelectedDate();
		getActionBar().setTitle(DateUtil.getMonthYear(selectedDate));

		int weekNum = selectedDate.get(Calendar.WEEK_OF_YEAR);

		// Sunday
		Calendar dateCal = Calendar.getInstance();
		dateCal.setTime(selectedDate.getTime());
		dateCal.set(Calendar.WEEK_OF_YEAR, weekNum);
		dateCal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

		sun_date_head = (TextView) findViewById(R.id.sun_date_head);
		mon_date_head = (TextView) findViewById(R.id.mon_date_head);
		tue_date_head = (TextView) findViewById(R.id.tue_date_head);
		wed_date_head = (TextView) findViewById(R.id.wed_date_head);
		thu_date_head = (TextView) findViewById(R.id.thu_date_head);
		fri_date_head = (TextView) findViewById(R.id.fri_date_head);
		sat_date_head = (TextView) findViewById(R.id.sat_date_head);

		sunday_box = (LinearLayout) findViewById(R.id.sunday_box);
		sun_date_head.setText(getWeekDate(dateCal, Calendar.SUNDAY));
		sunday_box.setOnClickListener(new WeekNavigationController(weekNum,
				Calendar.SUNDAY, sunday_box, eventAdapter));

		// Monday
		monday_box = (LinearLayout) findViewById(R.id.monday_box);
		mon_date_head.setText(getWeekDate(dateCal, Calendar.MONDAY));
		monday_box.setOnClickListener(new WeekNavigationController(weekNum,
				Calendar.MONDAY, monday_box, eventAdapter));
		// tuesday
		tue_date_head.setText(getWeekDate(dateCal, Calendar.TUESDAY));
		tuesday_box = (LinearLayout) findViewById(R.id.tuesday_box);
		tuesday_box.setOnClickListener(new WeekNavigationController(weekNum,
				Calendar.TUESDAY, tuesday_box, eventAdapter));

		// WEDNESDAY
		wed_date_head.setText(getWeekDate(dateCal, Calendar.WEDNESDAY));
		wednesday_box = (LinearLayout) findViewById(R.id.wednesday_box);
		wednesday_box.setOnClickListener(new WeekNavigationController(weekNum,
				Calendar.WEDNESDAY, wednesday_box, eventAdapter));
		// THURSDAY
		thu_date_head.setText(getWeekDate(dateCal, Calendar.THURSDAY));
		thursday_box = (LinearLayout) findViewById(R.id.thursday_box);
		thursday_box.setOnClickListener(new WeekNavigationController(weekNum,
				Calendar.THURSDAY, thursday_box, eventAdapter));

		// FRIDAY
		fri_date_head.setText(getWeekDate(dateCal, Calendar.FRIDAY));
		friday_box = (LinearLayout) findViewById(R.id.friday_box);
		friday_box.setOnClickListener(new WeekNavigationController(weekNum,
				Calendar.FRIDAY, friday_box, eventAdapter));
		// SATURDAY
		sat_date_head.setText(getWeekDate(dateCal, Calendar.SATURDAY));
		satday_box = (LinearLayout) findViewById(R.id.satday_box);
		satday_box.setOnClickListener(new WeekNavigationController(weekNum,
				Calendar.SATURDAY, satday_box, eventAdapter));
	}

	private String getWeekDate(Calendar dateCal, int day) {
		dateCal.set(Calendar.DAY_OF_WEEK, day);
		return String.valueOf(dateCal.get(Calendar.DATE));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.week_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_add:
			Intent addEvent = new Intent(this, EventFormActivity.class);
			addEvent.putExtra("requestCode", EventFormActivity.ADD_EVENT);
			startActivityForResult(addEvent, EventFormActivity.ADD_EVENT);
			break;
		case R.id.action_day_view:
			Intent dayView = new Intent(this, SocialEventPlannerActivity.class);
			startActivity(dayView);
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == EventFormActivity.ADD_EVENT
				|| resultCode == EventFormActivity.EDIT_EVENT_CODE) {

			WeekNavigationController.selectedBox.setBackground(defaultDrawable);
			populateWeekDates();
			eventAdapter.clear();
			eventList = SocialEventPlanner.getInstance().getEventModel().getEvents();
			eventAdapter.addAll(eventList);
			eventAdapter.notifyDataSetChanged();

		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle("MENU");
		menu.add(ContextMenu.NONE, 1, 0, "Edit");
		menu.add(ContextMenu.NONE, 2, 1, "Delete");

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
				.getMenuInfo();
		eventList = SocialEventPlanner.getInstance().getEventModel().getEvents();

		Event event = eventList.get(info.position);

		switch (item.getItemId()) {
		case 1:
			/**
			 * EDIT
			 */
			Intent addEvent = new Intent(this, EventFormActivity.class);
			addEvent.putExtra("requestCode", EventFormActivity.EDIT_EVENT_CODE);
			addEvent.putExtra("event", event);

			startActivityForResult(addEvent, EventFormActivity.EDIT_EVENT_CODE);
			return true;
		case 2:
			/**
			 * DELETE
			 */
			new DeleteEvent().execute(event);

			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	
	// SaveEvents() AsyncTask
		private class DeleteEvent extends AsyncTask<Event, Object, Event> {

			@Override
			protected Event doInBackground(Event... params) {
					db.removeEvent(params[0]);
				
				return null;
			}

			@Override
			protected void onPostExecute(Event e) {
				eventAdapter.clear();
				eventAdapter.addAll(db.getEvents());
				eventAdapter.notifyDataSetChanged();
				Toast.makeText(WeekViewActivity.this, R.string.event_removed,
						Toast.LENGTH_LONG).show();
			
			}
		}

}
