/**
 * Adapter to setup the attendee row
 */
package com.rmit.socialeventplanner.view.model;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.rmit.socialeventplanner.R;
import com.rmit.socialeventplanner.controller.AttendeeController;

public class AttendeeAdapter extends ArrayAdapter<String> {
	private LayoutInflater inflater = (LayoutInflater) getContext()
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	private List<String> list;
	public AttendeeAdapter(Activity activity, List<String> list) {
		super(activity, R.layout.attendee_row, list);
		this.list=list;

	}

	@Override
	public View getView(int position, View inflatedView, ViewGroup parent) {
		String contact = getItem(position);

		View row = inflater.inflate(R.layout.attendee_row, parent, false);
		TextView nameLabel = (TextView) row.findViewById(R.id.contact_name);
		nameLabel.setText(contact);
		ImageButton btn = (ImageButton) row.findViewById(R.id.del_btn);
		btn.setOnClickListener(new AttendeeController(contact, this.list, this));
		return row;
	}

}
