/**
 * An adapter to populate the event row
 */
package com.rmit.socialeventplanner.view.model;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rmit.socialeventplanner.R;
import com.rmit.socialeventplanner.controller.EventRowController;
import com.rmit.socialeventplanner.model.Event;
import com.rmit.socialeventplanner.util.DateUtil;
import com.rmit.socialeventplanner.view.SocialEventPlannerActivity;

public class EventAdapter  extends ArrayAdapter<Event>{
	private LayoutInflater inflater = (LayoutInflater) getContext()
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	private Activity activity;
	private int code;
	public EventAdapter(Activity activity, List<Event> list, int code) {
		super(activity, R.layout.event_list_row, list);
		this.activity = activity;
		this.code = code;

	}
	@Override
	public View getView(int position, View inflatedView, ViewGroup parent) {
		Event event = getItem(position);

		View row = inflater.inflate(R.layout.event_list_row, parent, false);
		TextView event_row_name = (TextView) row.findViewById(R.id.event_row_name);
		event_row_name.setText(event.getTitle());
		TextView event_row_date = (TextView) row.findViewById(R.id.event_row_date);
		event_row_date.setText(DateUtil.dateToString(event.getDate()));
		TextView event_row_attendees = (TextView) row.findViewById(R.id.event_row_attendees);
		List<String> attendees = event.getAttendees();
		int size  =0;
		if(attendees!=null){
			size=attendees.size();
		}
		event_row_attendees.setText("Attendees : "+size);
		if (code==SocialEventPlannerActivity.DAILY_CODE) {
			row.setOnClickListener(new EventRowController(activity, event));
		}
		return row;
	}
}
