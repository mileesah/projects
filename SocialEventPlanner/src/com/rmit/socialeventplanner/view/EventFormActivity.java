/**
 * Form to add and update events
 */
package com.rmit.socialeventplanner.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.rmit.socialeventplanner.R;
import com.rmit.socialeventplanner.controller.AttendeeController;
import com.rmit.socialeventplanner.controller.EventDateTxtController;
import com.rmit.socialeventplanner.controller.EventTimeTxtController;
import com.rmit.socialeventplanner.model.Event;
import com.rmit.socialeventplanner.model.dao.DBModel;
import com.rmit.socialeventplanner.util.DateUtil;
import com.rmit.socialeventplanner.view.model.AttendeeAdapter;

public class EventFormActivity extends Activity {

	private final int PICK_ATTENDEE_CODE = 8123;
	public static final int EDIT_EVENT_CODE = 124;
	public static final int ADD_EVENT = 10;

	private DBModel db;
	private EditText eventNameTxt;
	private EditText eventLocationTxt;
	private EditText eventDateTxt;
	private EditText eventTimeTxt;
	private EditText eventNoteTxt;
	private ListView contactList;
	private EditText eventVenueTxt;
	private List<String> attendees;
	private AttendeeController attendeeController;
	private int requestCode;
	private Date oldDate;
	private Event oldEvent;
	private MenuItem action_discard;
	private String action;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_form);

		db = new DBModel(EventFormActivity.this);
		requestCode = getIntent().getExtras().getInt("requestCode");
		eventNameTxt = (EditText) findViewById(R.id.event_name_txt);
		eventLocationTxt = (EditText) findViewById(R.id.event_location_txt);
		eventVenueTxt = (EditText) findViewById(R.id.event_venue_txt);

		eventNoteTxt = (EditText) findViewById(R.id.event_note_txt);
		eventDateTxt = (EditText) findViewById(R.id.event_date_txt);
		contactList = (ListView) findViewById(R.id.listAttendee);
		eventTimeTxt = (EditText) findViewById(R.id.event_time_txt);

		attendees = new ArrayList<String>();

		if (requestCode == EDIT_EVENT_CODE) {
			oldEvent = (Event) getIntent().getExtras().getSerializable("event");
			eventNameTxt.setText(oldEvent.getTitle());
			eventLocationTxt.setText(oldEvent.getLocation());
			eventVenueTxt.setText(oldEvent.getVenue());
			eventNoteTxt.setText(oldEvent.getNote());
			Calendar cal = Calendar.getInstance();
			oldDate = oldEvent.getDate();
			cal.setTime(oldDate);
			eventDateTxt.setText(DateUtil.formatCalendarDate(cal));
			eventTimeTxt.setText(DateUtil.formatCalendarTime(cal));
			attendees = oldEvent.getAttendees();
		}
		AttendeeAdapter attendeeAdapter = new AttendeeAdapter(this, attendees);

		EventDateTxtController controller = new EventDateTxtController(
				eventDateTxt, getFragmentManager());
		eventDateTxt.setOnFocusChangeListener(controller);
		eventDateTxt.setOnClickListener(controller);

		EventTimeTxtController timeController = new EventTimeTxtController(
				eventTimeTxt, getFragmentManager());
		eventTimeTxt.setOnFocusChangeListener(timeController);
		eventTimeTxt.setOnClickListener(timeController);

		contactList.setAdapter(attendeeAdapter);
		attendeeController = new AttendeeController(getContentResolver(),
				attendees, attendeeAdapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.form_menu, menu);
		action_discard = menu.findItem(R.id.action_discard);
		if (requestCode == ADD_EVENT) {
			action_discard.setVisible(false);

		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int id = item.getItemId();
		switch (id) {
		case R.id.action_save:
			action = "save";
			saveEvent();
			break;
		case R.id.action_add_contact:
			Intent attendeePicker = new Intent(Intent.ACTION_PICK,
					Contacts.CONTENT_URI);
			startActivityForResult(attendeePicker, PICK_ATTENDEE_CODE);
			break;
		case R.id.action_discard:
			action = "delete";
			Event e = new Event();
			new SaveEvent().execute(oldEvent);
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == PICK_ATTENDEE_CODE) {
			if (resultCode == RESULT_OK) {
				Uri contact = data.getData();
				attendeeController.updateAttendees(contact);
			}
		}
	}

	private Event getEvent() {
		Event e = new Event();
		if (oldEvent != null) {
			e = oldEvent;
		}
		e.setTitle(eventNameTxt.getText().toString().trim());
		e.setLocation(eventLocationTxt.getText().toString());
		e.setVenue(eventVenueTxt.getText().toString());
		String dateStr = eventDateTxt.getText().toString();
		String timeStr = eventTimeTxt.getText().toString();
		Date date = DateUtil.stringToDate(dateStr + " " + timeStr);

		e.setDate(date);

		e.setNote(eventNoteTxt.getText().toString());
		e.setAttendees(attendees);
		return e;

	}



	private void saveEvent() {
		Event e = getEvent();
		String errorMsg = validateEvent(e);
		if (errorMsg != null && errorMsg.length() > 0) {
			new AlertDialog.Builder(this)
					.setTitle("ERROR")
					.setMessage(errorMsg)
					.setPositiveButton(android.R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									// continue with delete
								}
							}).setIcon(android.R.drawable.ic_dialog_alert)
					.show();
		} else {

			boolean saveEvent;
			new SaveEvent().execute(e);

		}

	}

	// SaveEvents() AsyncTask
	private class SaveEvent extends AsyncTask<Event, Object, Event> {

		@Override
		protected Event doInBackground(Event... params) {
			if (action.equals("delete")) {
				db.removeEvent(params[0]);
			} else {
				if (requestCode == EDIT_EVENT_CODE) {
					db.updateEvent(params[0], oldDate);
				} else {
					db.addEvent(params[0]);
				}
			}

			return null;
		}

		@Override
		protected void onPostExecute(Event e) {
			setResult(requestCode);
			int msg =R.string.event_saved;
			if(action.equals("delete")){

				msg =R.string.event_removed;
			}	
			Toast.makeText(EventFormActivity.this, msg,
					Toast.LENGTH_LONG).show();
		
			finish();
		}
	}

	public String validateEvent(Event e) {
		StringBuilder errorMsg = new StringBuilder();
		if (e.getTitle() == null || e.getTitle().length() == 0) {
			errorMsg.append("Event Name is required.\n");
		}
		if (e.getDate() == null) {
			errorMsg.append("Invalid Date");

		}
		return errorMsg.toString().trim();

	}

}
