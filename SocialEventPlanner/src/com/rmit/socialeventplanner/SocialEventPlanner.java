package com.rmit.socialeventplanner;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.rmit.socialeventplanner.model.EventModel;

import android.app.Application;
import android.os.Bundle;
import android.util.Log;

public class SocialEventPlanner extends Application {
	
	private static SocialEventPlanner singleton;

	private EventModel eventModel;
	private Calendar selectedDate;
	private boolean isOpen;
	
	public EventModel getEventModel() { 
		return eventModel;
		
	}


	public boolean isOpen() {
		return isOpen;
	}
	


	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}



	public static SocialEventPlanner getInstance() {
		
		return singleton;
	}
	public Calendar getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Calendar selDate) {
		selectedDate = selDate;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		singleton = this;
		isOpen=false;
		this.selectedDate = Calendar.getInstance();
		eventModel = new EventModel();
		
	}

	
	

}
