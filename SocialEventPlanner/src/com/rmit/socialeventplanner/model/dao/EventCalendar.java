package com.rmit.socialeventplanner.model.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.rmit.socialeventplanner.model.Event;

/**
 * Object to store events
 * @author melissaferenal
 *
 */
public class EventCalendar extends HashMap<String, Map<Integer, List<Event>>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public boolean put(Event e) {

		try {
			String eventMonthKey = getEventMonthKey(e.getDate());
			Map<Integer, List<Event>> monthMap = this.get(eventMonthKey);

			if (monthMap == null) {
				monthMap = new TreeMap<Integer, List<Event>>();
			}

			Integer eventDayKey = getEventDayKey(e.getDate());
			List<Event> dayMap = monthMap.get(eventDayKey);
			if (dayMap == null) {
				dayMap = new ArrayList<Event>();
			}
			dayMap.add(e);
			monthMap.put(eventDayKey, dayMap);
			this.put(eventMonthKey, monthMap);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}

		return true;
	}
	
	public boolean update(Event e, Date oldDate){
		
		try {
			List<Event> events = getEvents(oldDate);
			events.remove(e);
			put(e);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
		
		return true;
	}

	private String getEventMonthKey(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.YEAR) + "_" + cal.get(Calendar.MONTH);
	}

	private Integer getEventDayKey(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DATE);
	}

	
	public boolean remove(Event e){
		List<Event> events = getEvents(e.getDate());
		events.remove(e);
		return true;
	}
	public List<Event> getEvents(Date date) {

		String monthKey = getEventMonthKey(date);

		Map<Integer, List<Event>> monthMap = this.get(monthKey);

		if (monthMap == null) {
			return new ArrayList<Event>();
		}

		Integer eventDayKey = getEventDayKey(date);
		List<Event> dayMap = monthMap.get(eventDayKey);
		if (dayMap == null) {
			return new ArrayList<Event>();
		}
		Collections.sort(dayMap);
		return dayMap;
	}

}
