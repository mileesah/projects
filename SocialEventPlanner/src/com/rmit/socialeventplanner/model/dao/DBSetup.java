package com.rmit.socialeventplanner.model.dao;

import java.util.Date;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBSetup extends SQLiteOpenHelper{

	// All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "socialeventplanner";
 
    // EVENTS table name
    public static final String TABLE_EVENTS = "events";
 
    // EVENTS Table Columns names
    public static final String EVENT_ID = "eventId";
    public static final String TITLE = "title";
    public static final String EVENT_DATE = "date";
    public static final String VENUE = "venue"; 
    public static final String LOCATION = "location";
    public static final String NOTE = "note";

    // attendees table name
    public static final String TABLE_ATTENDEES = "attendees";
    
    public static final String CONTACT_NAME = "contactNname";

    
    public DBSetup(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
	@Override
	public void onCreate(SQLiteDatabase db) {
		System.out.println("-----------setup");
		String CREATE_EVENTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_EVENTS + "("
                + EVENT_ID + " TEXT PRIMARY KEY," + TITLE + " TEXT,"+ EVENT_DATE + " TEXT,"
                + VENUE + " TEXT," + LOCATION + " TEXT," + NOTE + " TEXT)";
        db.execSQL(CREATE_EVENTS_TABLE);
        System.out.println(CREATE_EVENTS_TABLE);
        String CREATE_ATTENDEES_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_ATTENDEES + "("
                + EVENT_ID + " TEXT ," + CONTACT_NAME + " TEXT, "+
        		" PRIMARY KEY ("+EVENT_ID+","+CONTACT_NAME+ "), "
        		+"FOREIGN KEY ("+EVENT_ID+") REFERENCES "+TABLE_EVENTS+"("+EVENT_ID+"))";
        System.out.println(CREATE_ATTENDEES_TABLE);
        db.execSQL(CREATE_ATTENDEES_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ATTENDEES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
		 
        // Create tables again
        onCreate(db);
		
	}
	
}
