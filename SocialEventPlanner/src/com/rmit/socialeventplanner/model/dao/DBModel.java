package com.rmit.socialeventplanner.model.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.model.Event;
import com.rmit.socialeventplanner.model.EventModel;
import com.rmit.socialeventplanner.util.DateUtil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DBModel {

	
//    private static final String DB_NAME = "MyNotes";
//    private static final String TABLE_NAME = "tablenotes";
//    private static final String TITLE = "title";
//    private static final String ID = "_id";
//    private static final String NOTE = "note";
//    private static final int DATABASE_VERSION = 1;
	
	private EventModel eventsList;
    private SQLiteDatabase database;
    private DBSetup dbSetup;
 
    public DBModel(Context context) {
    	dbSetup = new DBSetup(context);
    	eventsList = SocialEventPlanner.getInstance().getEventModel();
    }
    
    
    public void open() throws SQLException {
        database = dbSetup.getWritableDatabase();
    }
 
    public void close() {
        if (database != null)
            database.close();
    }
 
    public Event addEvent(Event e) {
        ContentValues event = new ContentValues();
        UUID id = UUID.randomUUID();
        event.put(DBSetup.EVENT_ID, id.toString());
        event.put(DBSetup.TITLE, e.getTitle());
        event.put(DBSetup.EVENT_DATE, DateUtil.dateToString(e.getDate()));
        event.put(DBSetup.LOCATION, e.getLocation());
        event.put(DBSetup.VENUE, e.getVenue());
        event.put(DBSetup.NOTE, e.getNote());        
        open();
        database.insert(DBSetup.TABLE_EVENTS, null, event);
        
        if(e.getAttendees()!=null){
        	ContentValues v = new ContentValues();
        	v.put(DBSetup.EVENT_ID, id.toString());
	        for(String a : e.getAttendees()){
	        	v.put(DBSetup.CONTACT_NAME, a);
	        	database.insert(DBSetup.TABLE_ATTENDEES, null, v);
	        }
        }
        close();
        
        eventsList.saveEvent(e);
        return e;
    }
 
    
    public Event updateEvent(Event e, Date oldDate) {
        ContentValues event = new ContentValues();
        event.put(DBSetup.TITLE, e.getTitle());
        event.put(DBSetup.EVENT_DATE, DateUtil.dateToString(e.getDate()));
        event.put(DBSetup.LOCATION, e.getLocation());
        event.put(DBSetup.VENUE, e.getVenue());
        event.put(DBSetup.NOTE, e.getNote());        
        open();
        database.update(DBSetup.TABLE_EVENTS, event,DBSetup.EVENT_ID+"='"+e.getEventId()+"'", null);
        database.delete(DBSetup.TABLE_ATTENDEES, DBSetup.EVENT_ID+"='"+e.getEventId()+"'", null);
        
        if(e.getAttendees()!=null){
        	ContentValues v = new ContentValues();
        	v.put(DBSetup.EVENT_ID, e.getEventId());
	        for(String a : e.getAttendees()){
	        	v.put(DBSetup.CONTACT_NAME, a);
	        	database.insert(DBSetup.TABLE_ATTENDEES, null, v);
	        }
        }
        close();
        
        eventsList.update(e, oldDate);
        return e;
    }

    // Delete Database function
    public void removeEvent(Event e) {
        open();
        database.delete(DBSetup.TABLE_ATTENDEES, DBSetup.EVENT_ID+"='"+e.getEventId()+"'", null);
        database.delete(DBSetup.TABLE_EVENTS, DBSetup.EVENT_ID+"='"+e.getEventId()+"'", null);
        close();
        eventsList.remove(e);

    }
 
    // List all data function
    public void pupolateAllEvents() {
      open();

    	String selectQuery = "SELECT "+DBSetup.EVENT_ID+","+DBSetup.TITLE+","+DBSetup.EVENT_DATE+","+
    			DBSetup.LOCATION+","+DBSetup.VENUE+","+DBSetup.NOTE+
    			" FROM "+DBSetup.TABLE_EVENTS;
    	Cursor cursor = database.rawQuery(selectQuery, null);
    	while (cursor.moveToNext()) {
    		Event e = new Event();
    		e.setEventId(cursor.getString(0));
    		e.setTitle(cursor.getString(1));
    		e.setDate(DateUtil.stringToDate(cursor.getString(2)));
    		e.setLocation(cursor.getString(3));
    		e.setVenue(cursor.getString(4));
    		e.setNote(cursor.getString(5));
    		String s = "SELECT "+DBSetup.CONTACT_NAME+" FROM "+DBSetup.TABLE_ATTENDEES+" WHERE "+
    					DBSetup.EVENT_ID +" = '"+e.getEventId()+"'";
    		Cursor c =  database.rawQuery(s, null);
    		List<String> attendees = new ArrayList<String>(); 
    		while (c.moveToNext()) {
        		attendees.add(c.getString(0));
        	}
    		e.setAttendees(attendees);
    		eventsList.saveEvent(e);
    		c.close();
    	}
    	cursor.close();
    	close();
    }
 
    
    public List<Event> getEvents() {
		
		return eventsList.getEvents();

	}
    
 
}
