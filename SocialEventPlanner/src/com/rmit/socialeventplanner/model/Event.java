/**
 * @author Melissa Ferenal
 *
 * Model class that represents an event in the calendar
 * Implemented Comparable interface for sorting purposes
 * 
 */
package com.rmit.socialeventplanner.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Event implements Comparable<Event>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Event() {
		//eventId = UUID.randomUUID();
	}

	private String eventId;
	private String title;
	private Date date;
	private String venue;
	private String location;
	private String note;
	private List<String> attendees;

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<String> getAttendees() {
		return attendees;
	}

	public void setAttendees(List<String> attendees) {
		this.attendees = attendees;
	}

	@Override
	public int compareTo(Event another) {
		return this.date.compareTo(another.getDate());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (eventId == null) {
			if (other.eventId != null)
				return false;
		} else if (!eventId.equals(other.eventId))
			return false;

		return true;
	}

}
