/**
 * A manager that handles different operations regarding the event entity
 */

package com.rmit.socialeventplanner.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.model.dao.EventCalendar;

public class EventModel {

	private EventCalendar eCalendar;

	public EventModel() {
		this.eCalendar = new EventCalendar();
	}





	public List<Event> getEvents() {
		List<Event> events = eCalendar.getEvents(SocialEventPlanner.getInstance().getSelectedDate().getTime());
		return events;

	}

	public boolean saveEvent(Event e) {
		return eCalendar.put(e);
	}

	public boolean update(Event e, Date oldDate) {
		return eCalendar.update(e, oldDate);
	}

	public boolean remove(Event e) {
		return eCalendar.remove(e);
	}

	

}
