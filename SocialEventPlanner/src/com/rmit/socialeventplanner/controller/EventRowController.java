/**
 * Launches the edit form 
 */
package com.rmit.socialeventplanner.controller;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.rmit.socialeventplanner.model.Event;
import com.rmit.socialeventplanner.view.EventFormActivity;

public class EventRowController implements OnClickListener {

	private Activity activity;
	private Event event;

	public EventRowController(Activity activity, Event event) {
		this.activity = activity;
		this.event = event;
	}

	@Override
	public void onClick(View v) {
		Intent addEvent = new Intent(activity, EventFormActivity.class);
		addEvent.putExtra("requestCode", EventFormActivity.EDIT_EVENT_CODE);
		addEvent.putExtra("event", event);

		activity.startActivityForResult(addEvent, EventFormActivity.EDIT_EVENT_CODE);
	}

}
