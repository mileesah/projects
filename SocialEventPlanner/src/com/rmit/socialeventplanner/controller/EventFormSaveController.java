/**
 * Saves the event on click event
 */

package com.rmit.socialeventplanner.controller;

import java.util.List;

import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.model.Event;
import com.rmit.socialeventplanner.model.EventModel;
import com.rmit.socialeventplanner.view.SocialEventPlannerActivity;
import com.rmit.socialeventplanner.view.model.EventAdapter;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;

public class EventFormSaveController implements OnClickListener{

	private Event event;
	private EventModel eventModel;
	private Activity activity;
	public EventFormSaveController(Event e, Activity activity){
		this.event=e;
		eventModel =SocialEventPlanner.getInstance().getEventModel();
		this.activity=activity;
	}
	
	@Override
	public void onClick(View v) {
		
		eventModel.saveEvent(event);
		activity.finish();
	}
	
	

}
