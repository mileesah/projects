/**
 * Launches the time picker widget on click event
 */
package com.rmit.socialeventplanner.controller;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

import com.rmit.socialeventplanner.view.EventTimePicker;

public class EventTimeTxtController implements OnFocusChangeListener, OnClickListener{
	private EditText timeTxt;
	private FragmentManager fragmentManager;
	
	public EventTimeTxtController(EditText timeTxt, FragmentManager fragmentManager){
		this.timeTxt =timeTxt;
		this.fragmentManager = fragmentManager;
	}
	
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if(hasFocus){
			showFragment();		

		}
	}

	@Override
	public void onClick(View v) {
		showFragment();		
	}
	
	private void showFragment(){
		FragmentTransaction fragTransaction =fragmentManager.beginTransaction();
		DialogFragment eventDateFragment = new EventTimePicker(timeTxt);
		eventDateFragment.show(fragTransaction, "event_date_picker");

	}
	
	


}
