/**
 * Controller for event date form
 * sets the string equivalent of the selected date 
 */
package com.rmit.socialeventplanner.controller;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.DatePickerDialog.OnDateSetListener;
import android.widget.DatePicker;
import android.widget.EditText;

import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.model.EventModel;
import com.rmit.socialeventplanner.util.DateUtil;

public class DatePickerController implements OnDateSetListener {
	
	private EditText dateTxt;
	public DatePickerController(EditText dateTxt){
		this.dateTxt=dateTxt;
	}
	

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar selectedDate = new GregorianCalendar(year, monthOfYear, dayOfMonth);
		SocialEventPlanner.getInstance().setSelectedDate(selectedDate);
		dateTxt.setText(DateUtil.formatCalendarDate(selectedDate));
	}

}
