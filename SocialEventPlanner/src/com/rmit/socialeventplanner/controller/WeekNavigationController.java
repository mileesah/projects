/**
 * Controller class to handle change date navigation on the weekly view
 */

package com.rmit.socialeventplanner.controller;


import java.util.Calendar;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.model.EventModel;
import com.rmit.socialeventplanner.util.DateUtil;
import com.rmit.socialeventplanner.view.WeekViewActivity;
import com.rmit.socialeventplanner.view.model.EventAdapter;

public class WeekNavigationController implements OnClickListener {

	private Calendar date;
	private LinearLayout box;
	private EventModel instance;
	private EventAdapter EVENT_ADAPTER;
	public static LinearLayout selectedBox;

	public WeekNavigationController(int week, int dayOfWeek, LinearLayout box,
			EventAdapter eventAdapter) {
		this.instance = SocialEventPlanner.getInstance().getEventModel();

		EVENT_ADAPTER = eventAdapter;

		date = Calendar.getInstance();
		date.set(Calendar.WEEK_OF_YEAR, week);
		date.set(Calendar.DAY_OF_WEEK, dayOfWeek);
		this.box = box;
		setSelectedHeaderDate(dayOfWeek,
				SocialEventPlanner.getInstance().getSelectedDate().get(Calendar.DAY_OF_WEEK), box);

	}

	private void setSelectedHeaderDate(int dayOfWeek, int selectedDayOftheWeek,
			LinearLayout box) {
		if (dayOfWeek == selectedDayOftheWeek) {
			selectedBox = box;
			box.setBackground(WeekViewActivity.selectedDrawable);
		}
	}

	@Override
	public void onClick(View v) {

		if (!DateUtil.formatCalendarDateToHeaderString(date).equals(
				DateUtil.formatCalendarDateToHeaderString(SocialEventPlanner.getInstance()
						.getSelectedDate()))) {

			selectedBox.setBackground(WeekViewActivity.defaultDrawable);
			SocialEventPlanner.getInstance().setSelectedDate(date);
			box.setBackground(WeekViewActivity.selectedDrawable);
			selectedBox = box;
			EVENT_ADAPTER.clear();
			EVENT_ADAPTER.addAll(instance.getEvents());
			EVENT_ADAPTER.notifyDataSetChanged();
		}
	}

}
