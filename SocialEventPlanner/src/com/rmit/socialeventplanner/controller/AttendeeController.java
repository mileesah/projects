package com.rmit.socialeventplanner.controller;

import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;

public class AttendeeController implements OnClickListener{
	
	private ContentResolver cResolver;
	private List<String> attendees;
	private ArrayAdapter<String> adapter;
	private String selectedContact;
	
	public AttendeeController(ContentResolver cResolver,
			List<String> attendees,  ArrayAdapter<String> adapter){
		this.cResolver=cResolver;
		this.attendees=attendees;
		this.adapter=adapter;
	}
	
	public AttendeeController(String selectedContact,
			List<String> attendees,  ArrayAdapter<String> adapter){
		this.selectedContact=selectedContact;
		this.attendees=attendees;
		this.adapter=adapter;
	}
	
	//get name from selected contact and add to list of attendees
	public void updateAttendees(Uri contact) {
		String contactName = ContactsContract.Contacts.DISPLAY_NAME;

		Cursor result = cResolver.query(contact, null, null,
				null, null);
		if (result.getCount() > 0) {
			while (result.moveToNext()) {
				String name = result.getString(result.getColumnIndex( contactName ));
				//add contact if it does not exist in list
				if(!attendees.contains(name)){
					attendees.add(name);
					adapter.notifyDataSetChanged();
				}
			}
		}
	}


	//onclick handler when user clicks the remove button
	@Override
	public void onClick(View v) {
		attendees.remove(selectedContact);
		adapter.notifyDataSetChanged();
	}
}
