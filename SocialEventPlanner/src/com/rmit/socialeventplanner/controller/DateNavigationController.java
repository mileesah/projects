/**
 * Listener for navigation buttons on Daily view
 * Increments 1 day to the selected date when right navigation is pressed
 * Decrement 1 day to the selected date when left navigation is pressed
 */
package com.rmit.socialeventplanner.controller;

import java.util.Calendar;

import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.model.EventModel;
import com.rmit.socialeventplanner.util.DateUtil;
import com.rmit.socialeventplanner.view.model.EventAdapter;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class DateNavigationController implements OnClickListener {

	public static final int LEFT_NAVIGATION = 1;
	public static final int RIGHT_NAVIGATION = 2;
	private EventAdapter eventAdapter;
	private TextView titleHeader;
	private int operation;
	private EventModel eventModel;
	
	

	public DateNavigationController(EventAdapter eventAdapter,
			TextView titleHeader, EventModel eventModel, int operation) {
				this.eventAdapter = eventAdapter;
				this.titleHeader = titleHeader;
				this.eventModel = eventModel;
				this.operation = operation;

	}

	@Override
	public void onClick(View v) {
		Calendar selectedDate = SocialEventPlanner.getInstance().getSelectedDate();
		if(operation==LEFT_NAVIGATION){
			selectedDate.add(Calendar.DATE, -1);
		}else{
			selectedDate.add(Calendar.DATE, 1);
		}
		SocialEventPlanner.getInstance().setSelectedDate(selectedDate);
		titleHeader.setText(DateUtil.formatCalendarDateToHeaderString(selectedDate));
		eventAdapter.clear();
		eventAdapter.addAll(SocialEventPlanner.getInstance().getEventModel().getEvents());
		eventAdapter.notifyDataSetChanged();
	}

}
