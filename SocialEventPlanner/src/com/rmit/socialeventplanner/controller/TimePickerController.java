package com.rmit.socialeventplanner.controller;

/**
 * Sets the string equivalent of the selected time of the event
 */
import java.util.Calendar;

import android.app.TimePickerDialog.OnTimeSetListener;
import android.widget.EditText;
import android.widget.TimePicker;

import com.rmit.socialeventplanner.SocialEventPlanner;
import com.rmit.socialeventplanner.model.EventModel;
import com.rmit.socialeventplanner.util.DateUtil;

public class TimePickerController implements OnTimeSetListener {
	
	private EditText timeTxt;
	public TimePickerController(EditText timeTxt){
		this.timeTxt=timeTxt;
	}
	

	
	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		EventModel instance = SocialEventPlanner.getInstance().getEventModel();
		Calendar selectedDate = SocialEventPlanner.getInstance().getSelectedDate();
		selectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
		selectedDate.set(Calendar.MINUTE, minute);
		SocialEventPlanner.getInstance().setSelectedDate(selectedDate);
		timeTxt.setText(DateUtil.formatCalendarTime(selectedDate));
		
	}

}


