/**
 * Loads the date picker widget on click event
 */
package com.rmit.socialeventplanner.controller;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

import com.rmit.socialeventplanner.view.EventDatePicker;

public class EventDateTxtController implements OnFocusChangeListener, OnClickListener {
	
	private EditText dateTxt;
	private FragmentManager fragmentManager;
	
	public EventDateTxtController(EditText dateTxt, FragmentManager fragmentManager){
		this.dateTxt =dateTxt;
		this.fragmentManager = fragmentManager;
	}
	
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if(hasFocus){
			showFragment();		

		}
	}

	@Override
	public void onClick(View v) {
		showFragment();		
	}
	
	private void showFragment(){
		FragmentTransaction fragTransaction =fragmentManager.beginTransaction();
		DialogFragment eventDateFragment = new EventDatePicker(dateTxt);
		eventDateFragment.show(fragTransaction, "event_date_picker");

	}

}
